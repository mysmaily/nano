class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
    	t.attachment :image
      t.string :title
      t.text :content
      t.references :category, index: true
      t.timestamps null: false
    end
  end
end
